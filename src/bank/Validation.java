package bank;

public interface Validation 
{
	public Boolean validateSSN(String SSN);
	public Boolean validateAge(int age);
}
