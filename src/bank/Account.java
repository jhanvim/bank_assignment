package bank;

public abstract class Account 
{
	public long accountId;
	public String description;
	public double minimumBalance;
	
	public abstract void display();
}
