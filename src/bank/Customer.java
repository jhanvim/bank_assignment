package bank;
import java.util.*;
public class Customer implements Validation
{
    public String name,userName,password,SSN,email,address,captcha;
    public char[] arr=new char[5];
    public int age,ind;
    public long phone;
    public double balance;
    ArrayList<String> userList=new ArrayList<String>();
    ArrayList<String> passList=new ArrayList<String>();
    Scanner sc=new Scanner(System.in);
    public void login()
    {
        int flag=0;
        do
        {
        	System.out.println("Enter Username");
            userName=sc.nextLine();
            System.out.println("Enter Password");
            password=sc.nextLine();
            Random r=new Random();
            for(int i=0;i<5;i++)
            {
                arr[i]=(char)(r.nextInt(26)+'a');
            }
            System.out.println("\n"+String.valueOf(arr)+"\n");
        	System.out.println("Enter Captcha");
            captcha=sc.nextLine();
            ind=userList.indexOf(userName);
            if(ind!=-1)
            {
            	String pass=passList.get(ind);
            	if(pass.equals(password))
				{
            		if(String.valueOf(arr).equals(captcha))
                    {
                        System.out.println("Login Successful");
                        flag=0;
                    }
                    else
                    {
                        System.out.println("Invalid Captcha");
                        flag=1;
                    }
				}
            	else
                {
                    System.out.println("Invalid Password");
                    flag=1;
                }
            }
            else
            {
                System.out.println("Invalid Username");
                flag=1;
            }
            
        }
        while(flag==1);
    }
    
    public void deposit()
    {
    	System.out.println("\nEnter amount to deposit\n");
    	double depo=sc.nextDouble();
    	balance+=depo;
    	System.out.println("Transaction Successful");
    }
    
    public void withdrawal()
    {
    	System.out.println("\nEnter amount to withdraw\n");
    	double draw=sc.nextDouble();
    	balance-=draw;
    	System.out.println("Transaction Successful");
    }
    
    public void checkBalance()
    {
    	System.out.println("Your Balance is "+balance);
    }
    
    public void editProfile()
    {
    	System.out.println("Enter your Name");
        name=sc.nextLine();
        System.out.println("Enter your age");
        age=sc.nextInt();
        sc.nextLine();
        System.out.println("Enter your SSN");
        SSN=sc.nextLine();
        System.out.println("Enter your Address");
        address=sc.nextLine();
        System.out.println("Enter your Email");
        email=sc.nextLine();
        System.out.println("Enter your Phone Number");
        phone=sc.nextLong();
        if(validateSSN(SSN))
        {
        	if(validateAge(age))
        	{
        		System.out.println("\nYour Profile Updated\n");
                System.out.println("\nUpdated Details\n");
                System.out.println("Name: "+name);
                System.out.println("Age: "+age);
                System.out.println("Address: "+address);
                System.out.println("SSN: "+SSN);
                System.out.println("Email: "+email);
                System.out.println("Phone Number: "+phone);
        	}
        	else
        	{
        		System.out.println("Invalid Age Your Details are not Updated");
        	}
        }
        else
        {
        	System.out.println("Invalid SSN Your Details are not Updated");
        }
    }
    
    public void changePassword()
    {
    	int flag=0;
    	do
    	{
    		System.out.println("Enter old Password");
            String oldPassword=sc.nextLine();
            System.out.println("Enter new Password");
            String newPassword=sc.nextLine();
            if(password.equals(oldPassword))
            {
            	System.out.println("Your Password Updated");
            	flag=1;
            	passList.set(ind, newPassword);
            	System.out.println(passList);
            }
            else
            {
            	System.out.println("Invalid Old Password");
            }
    	}while(flag!=1);
    }
    public Boolean validateSSN(String SSN)
    {
    	if(SSN.length()==9)
    	{
    		return true;
    	}
    	return false;
    }
    public Boolean validateAge(int age)
    {
    	if(age>18)
    	{
    		return true;
    	}
    	return false;
    }

	@Override
	public String toString() {
		return "Customer [name=" + name + ", userName=" + userName + ", password=" + password + ", SSN=" + SSN
				+ ", email=" + email + ", address=" + address + ", age=" + age + ", phone=" + phone + ", balance="
				+ balance + "]";
	}
}
