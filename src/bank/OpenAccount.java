package bank;

import java.util.Scanner;

public class OpenAccount {

	String type;
	Scanner sc=new Scanner(System.in);
	public void openAcc() 
	{
		Account acc=new Saving();
		int flag=0;
        do
        {
        	System.out.println("\nEnter Account Id");
            acc.accountId=sc.nextLong();
            sc.nextLine();
            System.out.println("Enter description");
            acc.description=sc.nextLine();
            if(acc.accountId==101 && acc.description.equalsIgnoreCase("Savings"))
            {
            	Saving s=new Saving(101,"Savings");
            	type="Savings";
                s.display();
                flag=1;
            }
            else if(acc.accountId==102 && acc.description.equalsIgnoreCase("Current"))
            {
            	Current c=new Current(102,"Current");
            	type="Current";
                c.display();
                flag=1;
            }
            else
            {
            	System.out.println("Invalid Account id or Account Description");
            	flag=0;
            }
        }while(flag==0);
	}

}
