package bank;

public class Current extends Account
{
	Current(int id,String type)
	{
		accountId=id;
		description=type;
		minimumBalance=0;
	}
	public void display()
	{
		System.out.println("Account Id:"+accountId+"\nDescription: "+description);
		System.out.println("Current Account minimum balance is "+minimumBalance);
	}
}
